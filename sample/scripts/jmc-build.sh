#!/usr/bin/env bash

# install Oracle JDK 8 -> it contains JavaFX. JavaFX is needed to build JMC.
# build according to https://github.com/JDKMissionControl/jmc
# switch to java 11 (java home)
# launch: https://github.com/JDKMissionControl/jmc

# on my pc:
# cd /home/pawel/dev/git/jmc
# target/products/org.openjdk.jmc/linux/gtk/x86_64/jmc -vm $JAVA_HOME/bin