#!/usr/bin/env bash
# get https://gitpitch.com/pitchme/offline/bitbucket/pzalejko/jug-java-landscape/master/white/PITCHME.zip?p=jug2019
# unzip PITCHME.zip
# go to PITCHME/PITCHME
# launch
docker run -p 80:80 -v "$(pwd):/usr/share/nginx/html" nginx
# open http://localhost