#!/bin/sh

# it's my custom command to switch to java11 in bash.
#java11

mvn clean package

echo "Let's start..."
mvn -f pom.xml -Dgatling.simulationClass=jug.Test gatling:test
echo "--- FINISHED ---"
sleep 5
