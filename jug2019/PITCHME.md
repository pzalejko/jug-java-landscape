---?image=assets/img/matrix.jpeg
@title[Java in 2019]

# Java in 2019
#### Where we’ve been and where we’re going

<br><br>
##### Paweł Żalejko <br>[@fa[twitter]@pzalejko](https://twitter.com/pzalejko)<br>

---

### @fa[rocket] Topics for today @fa[rocket]

1. Recent changes
2. Different distributions and support
3. Tough decisions ahead
4. Possible choices

Note:

- prezentacja bedzie o Javie, **JDK**
- troche o zmianach w **API**
- troche o dystrybucjach **JDK**
- wsparciu i releasach **JDK**

---
### The State of Java in 2018

Note:

- zanim prezentacja
- sprawdzmy jak to wygladalo na koniec **2018**
- **ankieta** na publice: kto java6,8,8+?

--- 
![November 2018](assets/img/java_versions_on_market.png)
<span class="smallText">https://www.baeldung.com/java-in-2018</span>

Note:

- jak to wygladalo na koniec **2018**
- ankiete wypelniko ok **5k** developerow
- dane na **listopad 2018**
- widac, ktora werja dominuje

--- 

# 84.7%
## Java 8

Note:

- wersje **6,9,10** ponizej 4 %

---

## January 2019
@snap[fragment]
#### End of Public Updates for Oracle JDK 8
---

# 1 day left!
@snapend

<span class="smallText">https://www.oracle.com/technetwork/java/javase/overview/index.html</span>


---

@snap[north-west span-45 fragment]
@box[bg-green text-white demo-box-pad](Java 8 (LTS).#Release - March 2014<br>End: <br>- January 2019<br>- December 2020<br>- Jun 2023<br>- September 2023)
@snapend

@snap[south-west span-45 fragment]
@box[bg-orange text-white demo-box-pad](Java 9.#Release - September 2017<br>End: March 2018)
@snapend

@snap[north-east span-45 fragment]
@box[bg-pink text-white demo-box-pad](Java 10.#Release - March 2018<br>End: September 2018)
@snapend

@snap[south-east  span-45 fragment]
@box[bg-blue text-white demo-box-pad](Java 11 (LTS).#Release - September 2019<br>End: <br>- March 2019*<br>- September 2023<br>- September 2022+)
@snapend

@snap[midpoint fragment]
![](assets/img/everything-looks-good-here.jpg)
@snapend

Note:

-  **End of Public Updates process**
- jaav 8 -> Oracle Commercial users, Oracle Personal Users, AdoptOpenJDK, Red Hat
- java 11 -> Oracle free, Oracle Commercial, AdoptOpenJDK

---

## There are many providers

Note:

- Oracle Commercial Users
- Oracle Personal Users
- OpenJDK, AdoptOpenJDK, Red Hat, IBM
- more and more

---?image=assets/img/matrix_question.jpg
@snap[north]
### The question is:
## Java 8
## or
## Java 11?
@snapend
Note:

- czy migrowac z java 8 ?

---

# Java 11

---
![](assets/img/new-features-speed.gif)

---
## Lots of changes

@ul[squares]
- <a href="https://www.slideshare.net/Pivotal/55-new-features-in-jdk-9-83961680">55 New Features in JDK 9</a>
- <a href="https://dzone.com/articles/109-new-features-in-jdk-10">109 New Features in JDK 10</a>
- <a href="https://www.azul.com/90-new-features-and-apis-in-jdk-11/">90 New Features in JDK 11</a>
@ulend  

Note:
- **254** in total!

---

## Many new methods in well-known classes

@ul[squares]

- Stream API
- Collection clases
- I/O classes
- String
- Optional
- and more...

@ulend  

---

### Let's start with small things:
```java
Map<String, Integer> map = Map.of("a", 1, "b", 2);

Optional<String> value = ...;
Consumer<String> consumer = ...;
Runnable runnable = ...;
value.ifPresentOrElse(consumer, runnable);

List<String> strings = List.of("", "a", " ", "b", "  ", "c"); 
List<String> result = strings.stream()
        .filter(Predicate.not(String::isBlank))
        .collect(Collectors.toList());

result.stream()
        .takeWhile(s -> !s.equals("c"))
        .forEach(System.out::println);
```

@[1]
@[3-6]
@[8]
@[10]
@[13-14]

Note:

- **of**  unmodifiable
- metoda of, takze na **stream**
- **negacja** oraz **isBlank**
- returns a new stream only of the elements that match the given predicate

---
#### Private method in interface:

```java
public interface Sample {

    default void sayHello(String message) {
        printMessage(message);
    }

    private void printMessage(String msg) {
        System.out.println(msg);
    }
}
```

@[1, 6-8, 10]

---
#### A new Process API:

```java
public class Sample {
    public static void main(String[] args) {
        printInfo(ProcessHandle.current());
        ProcessHandle.allProcesses().forEach(Sample::printInfo);
    }
    private static void printInfo(ProcessHandle processHandle) {
        System.out.println(processHandle.pid());
        System.out.println(processHandle.info().user());
        System.out.println(processHandle.info().command());
        System.out.println(processHandle.info().commandLine());
    }
}
```

@[3, 4]
@[7, 8, 9, 10]

Note:

- dostep do procesow w sytemie
- dostep do **pid**

---

#### Local variable type inference:

```java
public static void main(String[] args) {
    var var = "var everywhere ;)";
    var stream = Stream.of("a", "b", sample, "c");
    var listA = new ArrayList<String>(); // as ArrayList<String>
    // List<String> listB = new ArrayList<>();
    var listB = new ArrayList<>(); // as ArrayList<Object>
    var tmp = ()->"foo"; // will not compile!

    List<String> items = stream
            .filter((@Nonnull var f) -> f.equals(sample))
            .collect(Collectors.toList());

    for (var item : items) { System.out.println(item); }
}
```
@[2-7]
@[10]
@[13-15]

Note:

- Tylko zmienne lokalne
- Wartość zadeklarowana jako var **musi być od razu przypisana**
- jako **parametry w stream**
- Technically, var **is not a keyword**, but a **reserved type name**
- benefit of 'var' in lambdas is annotations can be applied without losing brevity

---
#### A new HTTP client:

```java
public static void main(String[] args) {
    HttpClient client = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2) // default
            .build();
    HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("http://openjdk.java.net/"))
            .GET()   // default
            .build();
    client.sendAsync(request, BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .thenAccept(System.out::println)
            .join();
    }
```

@[2-4]
@[5-8]
@[9-12]

Note:

- native support for HTTP 1.1/2, WebSocket
- if not supported -> downgraded to HTTP/1.1
- HTTP 2: Single Connection to the server
- HTTP 2: Multiplexing. Multiple requests are allowed at the same time, on the same connection
- Server Push
- Binary format. More compact.

---
### GC and memory:

@ul[squares]
- JEP 307: Parallel Full GC for G1
- JEP 310: Application Class-Data Sharing
- JEP 318: Epsilon: A No-Op GC
- JEP 333: ZGC: A Scalable Low-Latency GC
- JEP 189: Shenandoah: A Low-Pause-Time GC @color[#DC143C]( \*\* )
@ulend

Note:
- **318** for testing. memory allocation without memory reclamation mechanism.
- **333** GC pause times should not exceed 10ms. Developed by Oracle, open sourced.
- **333** Pause times do not increase with the heap size
- **189** for java 12, Red Hat -> 8 and 11!

<!-- The G1 garbage collector is designed to avoid full collections, but when the concurrent collections can't reclaim memory fast enough a fall back full GC will occur. The current implementation of the full GC for G1 uses a single threaded mark-sweep-compact algorithm. We intend to parallelize the mark-sweep-compact algorithm and use the same number of threads as the Young and Mixed collections do. The number of threads can be controlled by the -XX:ParallelGCThreads option, but this will also affect the number of threads used for Young and Mixed collections. -->


<!-- the JVM needs to perform a couple of preparatory steps. Given a class name, it looks the class up in a JAR, loads it, verifies the bytecode, and pulls it into an internal data structure -->

<!-- Develop a GC that handles memory allocation but does not implement any actual memory reclamation mechanism. Once the available Java heap is exhausted, the JVM will shut down.

 -->

---

## Docker
@ul[squares]
- *namespaces* - isolates from other processes
- *cgroups* - limits the resource consumption

@ulend
@snap[ fragment]
##### The problem ...
```java
// Java8: it will not work as expected
docker container run -it -m=512M --cpus 2 ...
```

Note:
- namespaces - makes the containerized process ...
- cgroups - provides a facility to limit
--- 

## Java < 10
@snap[ fragment]
```shell
docker container run -it -m=512M --entrypoint bash openjdk:8
 #docker-java-home/bin/java -XX:+PrintFlagsFinal \ 
  -version | grep MaxHeapSize

uintx MaxHeapSize  := 4181721088   {product}
```

@[1]
@[2-3]
@[5]

Note:

- **PrintFlagsFinal** options HotSpot using for running

---

## Java 10+
@snap[ fragment]
```shell
docker container run -it -m=512M --entrypoint bash openjdk:11
 #docker-java-home/bin/java -XX:+PrintFlagsFinal \ 
  -version | grep MaxHeapSize

size_t MaxHeapSize   = 132120576  {product} {ergonomic}
```
@[1]
@[2-3]
@[5]

---

### Docker, CPU and Java
@snap[ fragment]
- --cpus 2 / --cpu-period / --cpu-quota
- --cpu-shares 1024
- --cpuset-cpus="1,2,3"
- <a href="https://bugs.openjdk.java.net/browse/JDK-8182070">Container Awareness API</a>

<a href="https://blog.docker.com/2018/04/improved-docker-container-integration-with-java-10" class="smallText">source1</a>
<a href="https://docs.docker.com/engine/reference/run/#/runtime-constraints-on-resource" class="smallText">source2</a>

Note:

- **cpu-shares** - By default, all containers get the same proportion of CPU cycles
- **cpu-shares** - proportion can be modified
- **cpuset-cpus** - CPUs in which to allow execution (0-3, 0,1)
- **JVM internals, netty, core.async, fork join,elasticsearch**
---

### and much more:

@ul[squares]
- Jigsaw
- JEP 282: jlink
- JEP 222: jshell
- JEP 332: Transport Layer Security (TLS) 1.3
- but..
@ulend

Note:

- jlink, default jre has **4300+** classes.
- jlink create our own customized JRE

---
# Java 11
### changes everything

---

## New release train
@ul[squares]
- time-based releases
- "feature" release every six months
- LTS release every three years
@ulend

---
![](assets/img/diagramm-oracle-1.png)
<a href="https://dev.karakun.com/java/2018/06/25/java-releases.html" class="smallText">source</a>

Note:
- to jest plan **Oracle**
- patrz na **LTS**

---

# Java LTS
## 8, 11, 17
---

## Oracle
@ul[squares]
- OpenJDK builds (GPLv2)
- Oracle JDK builds (commercial product!)
@ulend

---
@quote[From Java 11 forward, therefore, Oracle JDK builds and OpenJDK builds will be essentially identical. ...yet with some cosmetic and packaging differences]

<span class="smallText">https://blogs.oracle.com/java-platform-group/oracle-jdk-releases-for-java-11-and-later</span>

Note:

- poniewaz licecja **BCL** to mix **open source i commercial**
- Oracle JDK for non-commercial use is fine

@ulend
---

## Oracle LTS
@ul[squares]
- OpenJDK builds from Oracle are @color[#DC143C](not) LTS!
- Oracle JDK builds @color[#DC143C](are) LTS (commercial)
@ulend

---

## The Price

@ul[squares]
- Desktop pricing is @color[#DC143C]($2.50) per user per month
- Processor pricing for use on Servers and/or Cloud deployments is @color[#DC143C]($25.00) per month
- *"It also includes access to My Oracle Support (MOS) 24x7, support in 27 languages"*
- <a href="https://www.oracle.com/technetwork/java/javaseproducts/overview/javasesubscriptionfaq-4891443.html" >More info here</a>
- <a href="https://www.oracle.com/java/java-se-subscription.html">Oracle Java SE Subscriptions program</a>
@ulend

Note:

- subscription provides access to tested and certified performance
- stability, and security updates for Java SE, directly from Oracle

<!-- https://www.oracle.com/technetwork/java/java-se-support-roadmap.html
https://www.oracle.com/technetwork/java/javaseproducts/overview/javasesubscriptionfaq-4891443.html -->

---

## Usage of Oracle JDK after March 2019
### @color[#DC143C](requires a commercial license)

---

## Purges in Java
@ul[squares]
- <a href="https://openjdk.java.net/jeps/320">Removal of the Java EE and CORBA Modules</a>
- <a href="http://hg.openjdk.java.net/jmc/jmc/rev/a76a464b3764">Removal of Java Mission Control</a>
- <a href="https://openjdk.java.net/projects/openjfx/">Removal of JavaFX from JDK 11</a>
- <a href="http://openjdk.java.net/jeps/335">Deprecate the Nashorn JavaScript Engine</a>
- <a href="http://openjdk.java.net/jeps/335">Removal of the Java Plugin and Java WebStart</a>
@ulend

<a href="https://www.oracle.com/technetwork/java/javase/11-relnote-issues-5012449.html#Removed" class="smallText">source</a>

Note:
- czystka
- https://jakarta.ee/
- czy z powodu saportu doszlo do czystki?
- **mission control** Lays off 
- GraalVM as replacement
---

## What if I don't want Oracle JDK?

Note:
- OpenJDK is open sourced
- many companies contribute
- there are other ready-to-take builds

---

# OpenJDK
### https://openjdk.java.net/

---
## Red Hat

@ul[squares]
- OpenJDK 8 - June 2023 (commercial support)
- OpenJDK 11 - October 2024 (commercial support)
- Shenandoah GC is available for JDK 8u and 11u
- <a href="https://twitter.com/shipilev/status/1072898303556284417">JDK 11+ - both Shenandoah and ZGC</a>
- <a href="https://developers.redhat.com/products/openjdk/download/">you can download for Windows</a>
- <a href="https://twitter.com/neugens/status/1034839076044775426">an "upstream first" policy</a>

@ulend

Note:

- whatever Red Hat does for its Java support ends up both in OpenJDK
- all security patches are applied to upstream OpenJDK releases
- and in downstream distro binaries (Fedora, Centos, Amazon Linux, etc)
- deep integration with RHEL

---

## IBM

@ul[squares]
- <a href="https://developer.ibm.com/javasdk/downloads/sdk8/">Free IBM SDK for Java 8</a>
- <a href="https://developer.ibm.com/javasdk/2018/04/26/java-standard-edition-ibm-support-statement/">IBM will continue to update OpenJDK Java 8</a>
- <a href="https://developer.ibm.com/javasdk/2018/04/26/java-standard-edition-ibm-support-statement/">and they will do it for 4 years</a>

@ulend

Note:
- update with security patche
- nie znalazlem jdk 11 od IBM
- ale darmowe wsparcie dla java 8
- wsparcie adoptOpenJdk


<!-- https://developer.ibm.com/javasdk/support/ -->
<!-- https://developer.ibm.com/javasdk/2018/04/26/java-standard-edition-ibm-support-statement/ -->

<!-- There have been many changes recently in the Java world; the release cadence is changing and support lifetimes are changing. IBM is committed to working in the OpenJDK, Eclipse OpenJ9, and AdoptOpenJDK communities to ensure that secure, high quality binaries for all LTS Java releases are available freely to Java developers until one year beyond the release of the next LTS version. -->
---

## Azul Systems
@ul[squares]
- <a href="https://www.azul.com/downloads/zulu/">Zulu: Free build of OpenJDK</a>
- <a href="https://www.azul.com/downloads/zulu/zulufx/">Builds of Zulu With OpenJFX</a>
- <a href="https://www.azul.com/downloads/zulu/zulu-download-arm/">Builds of Zulu for 64-bit Armv8</a>
- <a href="https://azure.microsoft.com/pl-pl/blog/microsoft-and-azul-systems-bring-free-java-lts-support-to-azure/">Microsoft Azure uses Zulu Enterprise (LTS)</a>
- <a href="https://www.azul.com/products/azul_support_roadmap/">Zulu Enterprise</a>
 - Java 8 - 2026
 - Java 11 - 2027
@ulend

Note:
- Azul Enterprose provides LTS for Azure
- **Azure** you can use Zulu Enterprise for free
---

## Amazon Corretto

@ul[squares]
- In preview
- No-cost
- Corretto 8  - GA is planned for Q1 2019
- Corretto 8  - until at least June 2023
- Corretto 11 - GA during the first half of 2019
- Corretto 11 - until at least August 2024
@ulend

<a href="https://aws.amazon.com/blogs/opensource/amazon-corretto-no-cost-distribution-openjdk-long-term-support/" class="smallText">source</a>

Note:

- **license** - GPL2 with the Class Path Exception

---

## OpenJDK - DIY!

@ul[squares]
- <a href="https://github.com/p-zalejko/openjdk">turned out to be not so hard</a>  
 - Boot JDK
 - source code (Mercurial or Git)
 - tools (automake, gcc-c++, ... , Docker)
- 16 GB RAM, i7-4700MQ, SSD -> 10 minutes
- <a href="https://bitbucket.org/pzalejko/openjdk-builds/raw/66da16eeec57ac1a86acac58934bbd31e3a00aa7/openJDK-11.0.1.gz">My OpenJDK build;)</a>  
@ulend

Note:
- to jest open source
- zrobilo sie cieplo w pokoju ;)
- na koniec moge pokazac dockera itp.

---

## AdoptOpenJDK 
@ul[squares]
- https://adoptopenjdk.net/
- community-driven project
- provides prebuilt OpenJDK binaries
- <a href="https://adoptopenjdk.net/sponsors.html">sponsored and supported by many companies</a>
- Java 8 and Java 11
- JVM: HotSpot and OpenJ9
@ulend

Note:
-  from a fully open source set of build scripts and infrastructure

---

## JVM
@ul[squares]
- Responsibilities:
  - Loading, verifying, and executing the byte code
  - Providing a runtime environment
  - Memory management and garbage collection
- <a href="https://docs.oracle.com/javase/specs/">specification</a> 
- <a href="https://en.wikipedia.org/wiki/List_of_Java_virtual_machines">19 implementations!</a> 

@ulend

---

## JVM implementations
@ul[squares]
- <a href="https://www.oracle.com/technetwork/java/javase/tech/index-jsp-136373.html">HotSpot</a>
- <a href="https://www.eclipse.org/openj9/">OpenJ9</a> (<a href="https://www.ibm.com/support/knowledgecenter/en/SSYKE2_8.0.0/welcome/welcome_javasdk_version.html">IBM J9 VM</a>)
- <a href="https://www.graalvm.org/">GraalVM</a>
@ulend

Note:
- GraalVM is a universal virtual machine
- JavaScript, Python, Ruby, R, JVM
- Native images

---

## Examples

---

## @fa[star] Thank you @fa[star]
### Q&A Time


